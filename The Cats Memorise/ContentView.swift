//
//  ContentView.swift
//  The Cats Game
//
//  Created by entymon on 28/06/2020.
//  Copyright © 2020 anymouse. All rights reserved.
//

import SwiftUI

struct MemorizeGameView: View {
    var body: some View {
        HStack {
            Text("Hello, World!")
        }
    }
}

//struct

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MemorizeGameView()
    }
}


