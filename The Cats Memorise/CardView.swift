//
//  CardView.swift
//  The Cats Game
//
//  Created by Pawel Olejniczak on 01/07/2020.
//  Copyright © 2020 anymouse. All rights reserved.
//

import SwiftUI

struct CardView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView()
    }
}
