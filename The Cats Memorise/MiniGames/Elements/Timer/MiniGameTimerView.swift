import SwiftUI

struct MiniGameTimerView: View {
    
    var game: MemoriseModel
    let screen = UIScreen.main.bounds
    
    @State private var timeRemaining: CGFloat = 60.0
    
    var body: some View {
        ZStack() {
            
            Image("mini_game_timer_bottom")
                .resizable()
                .scaledToFit()
                .overlay(
                    TimerOverlayView(game: game, timeRemaining: $timeRemaining)
                )
                
            Image("mini_game_timer_top")
                .resizable()
                .scaledToFit()
        }
    }
}

struct MiniGameTimerView_Previews: PreviewProvider {
    static var previews: some View {
        MiniGameTimerView(game: MemoriseModel())
    }
}
