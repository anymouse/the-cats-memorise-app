//
//  MiniGameTimerViewModel.swift
//  The Cats Game
//
//  Created by entymon on 07/02/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class MiniGameTimerViewModel: ObservableObject {
    
    let totalSecond: CGFloat = 60.0
    let deafultMargin: CGFloat = 8
    
    func offsetResize(widthOfScreen fullWidth: CGFloat, secondWithinMinute stepValue: CGFloat) -> CGFloat {
        if (stepValue > totalSecond || stepValue < 0) {
            return 0.0
        }
        
        // Left & Right margin = 8%
        let leftMargin = ((fullWidth / 100) * deafultMargin)
        
        let convertMinuteToWidth = fullWidth - (fullWidth / 100) * deafultMargin * 2
        let secondsByWidth = (convertMinuteToWidth * stepValue) / totalSecond
        
        return -(leftMargin + (secondsByWidth))
    }
}
