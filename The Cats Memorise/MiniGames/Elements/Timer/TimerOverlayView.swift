//
//  TimerOverlayView.swift
//  The Cats Game
//
//  Created by entymon on 07/02/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import SwiftUI

struct TimerOverlayView: View {
    
    let vm = MiniGameTimerViewModel()
    let game: MemoriseModel
    
    @Binding var timeRemaining: CGFloat
    
    let timer = Timer.publish(every: 1, tolerance: 0.5, on: .main, in: .common).autoconnect()
    
    var body: some View {
        
        GeometryReader { geometry in
            VStack {
                Image("mini_game_timer_colours")
                    .resizable()
                    .frame(width: geometry.size.width, height: geometry.size.height)
                    .mask(
                        Rectangle()
                            .frame(
                                width: geometry.size.width,
                                height: geometry.size.height,
                                alignment: .leading
                            )
                            .offset(
                                x: vm.offsetResize(widthOfScreen: geometry.size.width, secondWithinMinute: self.timeRemaining),
                                y: 0
                            )
                            .animation(Animation
                                        .linear(duration: 1))
                    )
                    .onReceive(timer) { time in
                        if self.timeRemaining > 0 && self.game.gameScore != self.game.gameWinnerScore {
                            self.timeRemaining -= CGFloat(1)
                        }
                        
                        if self.game.gameScore == self.game.gameWinnerScore {
                            timer.upstream.connect().cancel()
                        }
                        
                        if self.timeRemaining == 0 {
                            self.game.gameStatusPublisher?.send(true)
                            timer.upstream.connect().cancel()
                        }
                    }
            }
        }
    }
}

struct TimerOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            TimerOverlayView(game: MemoriseModel(), timeRemaining: .constant(32.0))
        }.frame(width: 400, height: 100, alignment: .leading)
        
    }
}
