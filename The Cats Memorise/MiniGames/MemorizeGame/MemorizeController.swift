import Foundation
import SwiftUI

class MemorizeController: ObservableObject {
    
    struct PairAttributes {
        let id: Int
        let attribute: Color
        let image: Image
    }
        
    var pairs: [PairAttributes] = []
    
    var attributes: [Color] = [
        MemorizeColors.blue,
        MemorizeColors.brown,
        MemorizeColors.darkBlue,
        MemorizeColors.darkGreen,
        MemorizeColors.darkRed,
        MemorizeColors.green,
        MemorizeColors.lightRed,
        MemorizeColors.lightBlue,
        MemorizeColors.lightBrown,
        MemorizeColors.lightGreen,
        MemorizeColors.orange,
        MemorizeColors.pink,
        MemorizeColors.purple,
        MemorizeColors.red,
        MemorizeColors.yellow
    ]
    
    init() {
        
        // legacy
        var identifier: Int = 0
        for attribute in attributes {
            
            identifier += 1
            pairs.append(PairAttributes(id: identifier, attribute: attribute, image: Image("memorise-card-\(identifier - 1)")))
            pairs.append(PairAttributes(id: identifier, attribute: attribute, image: Image("memorise-card-\(identifier - 1)")))
        }
        pairs.shuffle()
    }
    
    func getCardView() -> MemorizeCardView {
        let pair = pairs.removeFirst()
        let card = MemorizeCard(
            pairId: pair.id,
            pairAttribute: pair.attribute,
            pairImage: pair.image
        )
        
        return MemorizeCardView(game: self, card: card)
    }
    
    @Published var selectedPairId: Int = 0
    @Published var selectedCard: MemorizeCard = MemorizeCard()
    
    func matchCards (card: MemorizeCard) {
        if selectedCard !== card {
            if selectedPairId == 0 {
                selectedPairId = card.id
                selectedCard = card
            } else {
                if selectedPairId == card.id {
                    selectedCard.hide()
                    card.hide()
                } else {
                    let previousCard = selectedCard
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        previousCard.faceUp = false
                        card.faceUp = false
                    }
                }
                selectedPairId = 0
                selectedCard = MemorizeCard()
            }
        }
    }
}
