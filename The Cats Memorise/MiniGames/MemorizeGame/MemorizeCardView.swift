import SwiftUI

struct MemorizeCardView: View {
    @ObservedObject var game: MemorizeController
    @ObservedObject var card: MemorizeCard
    
    @ViewBuilder
    var body: some View {
        if !card.matched {
            Group {
                Button(action: {
                    if (!self.card.matched) {
                        self.card.faceUp.toggle()
                        self.game.matchCards(card: self.card)
                    }
                }) {
                    HStack{
                      Text("Card")
                    }
                    .padding(40)
                }
                .frame(width: 60.0, height: 80.0)
            }
            .background(card.faceUp ? card.attribute : Color.black)
        } else {
            Group {
                Button(action: {}) {
                    HStack{
                      Text("Card")
                    }
                    .padding(40)
                }
            }
            .frame(width: 60.0, height: 80.0)
            .background(Color.white)
        }
    }
}


struct MemorizeCardView_Previews: PreviewProvider {
    static var previews: some View {
        MemorizeCardView(game: MemorizeController(), card: MemorizeCard())
    }
}
