import SwiftUI

/**
 * Set of color for memorize game
 */
struct MemorizeColors {
    
    static let blue = Color("MemorizeGameBlue")
    static let brown = Color("MemorizeGameBrown")
    static let darkBlue = Color("MemorizeGameDarkBlue")
    static let darkGreen = Color("MemorizeGameDarkGreen")
    static let darkRed = Color("MemorizeGameDarkRed")
    static let green = Color("MemorizeGameGreen")
    static let lightBlue = Color("MemorizeGameLightBlue")
    static let lightBrown = Color("MemorizeGameLightBrown")
    static let lightGreen = Color("MemorizeGameLightGreen")
    static let lightRed = Color("MemorizeGameLightRed")
    static let orange = Color("MemorizeGameOrange")
    static let pink = Color("MemorizeGamePink")
    static let purple = Color("MemorizeGamePurple")
    static let red = Color("MemorizeGameRed")
    static let yellow = Color("MemorizeGameYellow")
}


