import SwiftUI

struct MemorizeView: View {
    @State private var showingGame: Bool = false
    
    var body: some View {
        if showingGame {
            MemorizeGameView()
        } else {
            MemorizeHomeScreenView(showingGame: $showingGame)
        }
    }
}

struct MemorizeHome_Previews: PreviewProvider {
    static var previews: some View {
        MemorizeView()
    }
}
