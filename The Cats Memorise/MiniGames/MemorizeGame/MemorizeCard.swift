import Foundation
import SwiftUI

class MemorizeCard: ObservableObject {
    var id: Int = 0
    @Published var faceUp: Bool = false
    
    @Published var attribute: Color = Color.white
    @Published var image: Image = Image("")
    @Published var matched: Bool = false
        
    init () {
        attribute = Color.white
    }
    
    init (pairId id: Int, pairAttribute attribute: Color, pairImage image: Image) {
        self.id = id
        self.attribute = attribute
        self.image = image
    }
        
    func hide () -> Void {
        self.attribute = Color.white
        self.image = Image("")
        self.matched = true
    }
}
