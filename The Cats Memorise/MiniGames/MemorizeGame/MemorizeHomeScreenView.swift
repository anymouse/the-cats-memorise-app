import SwiftUI

let text = """
Find 10 pairs of cards
to get a juicy, magic carrot
"""

/**
 Home screen for memorize game
 */
struct MemorizeHomeScreenView: View {
    @Binding var showingGame: Bool
    
    @State private var hovered = false
    @State private var imageButton = "memorise-btn-play-active"
    
    var body: some View {
        ZStack {
        
            Image("game-background")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                
                Spacer()
                
                Image("memorise-home-screen-title")
                    .padding(.bottom, 20)
                
                Image("memorise-home-screen-image")
                    .padding(.bottom, 20)
                
                Text(text)
                    .foregroundColor(.red)
                    .font(.custom("BubblegumSans-Regular", size: 28))
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 40)
                
                Image(imageButton)
                    .onTapGesture {
                        imageButton = "memorise-btn-play-pressed"
                        showingGame = true
                    }
                
                
//                Button(action: {
//                    showingGame = true
//                }, label: {
//                    Image(imageButton)
//                        .onTapGesture {
//                            imageButton = "memorise-btn-play-pressed"
//                            showingGame = true
//                        }
//                })
            
                Spacer()
            }
        }
    }
}

struct MemorizeHomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        MemorizeHomeScreenView(showingGame: .constant(false))
    }
}
