import SwiftUI
import GameController

struct MemorizeGameView: View {
    var game: MemorizeController = MemorizeController()
    
    var body: some View {
        HStack {
            ForEach(0 ..< 5) { positionX in
                VStack {
                    ForEach(0 ..< 6) { positionY in
                        self.game.getCardView().padding(4.0)
                    }
                }
            }
        }
    }
}

struct GameContent_Previews: PreviewProvider {
    static var previews: some View {
        MemorizeGameView()
    }
}
