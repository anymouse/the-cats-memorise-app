//
//  ViewModel.swift
//  The Cats Memorise
//
//  Created by entymon on 11/04/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import Foundation
import Combine



//struct Test {
//    var selectedCard: Int
//}

//enum State {
//
//    case selectedCard
//
//    var input: Test {
//            switch self {
//            case .selectedCard:
//                return Test(selectedCard: 1)
//        }
//    }
//}

class ViewModel: ObservableObject {
    var subscriptions = Set<AnyCancellable>()
    
    var store = MemoriseStore()
    
    var subject: CurrentValueSubject<Int, Never>
    
    init () {
        subject = CurrentValueSubject<Int, Never>(0)
        subject
            .print("publisher")
            // add new operator to change behaviour of app
            .sink(receiveValue: { print($0) })
            .store(in: &subscriptions)
    }
    
    func getSubject() {
        
    }
    
    deinit {
        self.subscriptions = []
    }
}

class MemoriseStore {
    var selectedCard: Int = 0
    var previousCard: Int = 0
    var match: Bool = false
}

