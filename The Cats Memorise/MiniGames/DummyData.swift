//
//  DummyData.swift
//  The Cats Memorise
//
//  Created by entymon on 11/04/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import Foundation
import Combine

let memoriseCardDummy = MemoriseCardVM(
    game: MemoriseModel(),
    pairId: 1,
    withFrontImageName: "memorise-card-1"
)
