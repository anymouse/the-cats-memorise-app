//
//  MemoriseWelcomeScreenView.swift
//  The Cats Memorise
//
//  Created by Pawel Olejniczak on 25/06/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import SwiftUI

struct MemoriseWelcomeScreenView: View {
    @State private var isClicked = false
    
    var game: MemoriseModel
    
    var body: some View {
        ZStack {
            Image("game-background")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                Image("memorise-home-screen-title")
                
                Image("memorise-home-screen-image")
                
                Text("Find 15 pairs od cards")
                    .font(Font.custom("BubblegumSans-Regular", size: 22))
                    .foregroundColor(Color("memorise-red"))
                
                Text("to get a juicy, magic carrot")
                    .font(Font.custom("BubblegumSans-Regular", size: 22))
                    .foregroundColor(Color("memorise-red"))
                
                Image(self.isClicked == false ? "memorise-btn-play-active" : "memorise-btn-play-pressed")
                  .onTapGesture {
                    self.isClicked.toggle()
                    self.game.gameStatusPublisher?.send(self.isClicked)
                  }
            }
        }
    }
}

struct MemoriseWelcomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        MemoriseWelcomeScreenView(game: MemoriseModel())
    }
}
