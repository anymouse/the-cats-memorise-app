//
//  MemoriseSubscription.swift
//  The Cats Memorise
//
//  Created by entymon on 12/04/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import Foundation
import Combine

class MemoriseSubscription {
    
    static var subscriptions = Set<AnyCancellable>()
    
    static func matchSubscription () -> PassthroughSubject<Bool?, Never> {
        let subject = PassthroughSubject<Bool?, Never>()
        subject
            .sink(receiveValue: { match in
                self.match = match
            })
            .store(in: &subscriptions)
        return subject
    }
}
