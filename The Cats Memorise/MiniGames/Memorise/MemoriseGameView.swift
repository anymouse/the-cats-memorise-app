import SwiftUI

@available(iOS 14.0, *)
struct MemoriseGameView: View {
    @ObservedObject var vm: MemoriseGameVM
        
    let layout = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    
    @State private var numberOfCards: Int = 30
    @State private var rowNumber: Int = 0
    
    var body: some View {
        ZStack {
            if vm.gameInPlayMode {
                Image("memorise-game-background")
                    .resizable()
                    .edgesIgnoringSafeArea(.all)
                
                VStack {
                    
                    VStack(alignment: .leading, spacing: 0) {
                        MemoriseScoreCounterView(game: vm.game)
                            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
                            .edgesIgnoringSafeArea(.all)
                    }
                    
                    Spacer()
                    
                    VStack(spacing: 10.0) {
                        LazyVGrid(columns: layout, content: {
                            ForEach(vm.getCards()) { card in
                                MemoriseCardView(vm: card)
                            }
                            Spacer()
                        })
                        .padding(16.0)
                    }
                    
                    Spacer()
                    
                    MiniGameTimerView(game: vm.game)
                }
            } else {
                MemoriseWelcomeScreenView(game: vm.game)
            }
        }
    }
}

@available(iOS 14.0, *)
struct MemoriseGameView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.gray
            
            MemoriseGameView(vm: MemoriseGameVM())
        }
    }
}
