//
//  MemoriseScoreCounterView.swift
//  The Cats Memorise
//
//  Created by entymon on 14/04/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import SwiftUI

struct MemoriseScoreCounterView: View {
    
    @ObservedObject var vm: MemoriseScoreCounterVM
    
    init (game gameModel: MemoriseModel) {
        vm = MemoriseScoreCounterVM(gameScore: gameModel.$gameScore.eraseToAnyPublisher())
    }
    
    var body: some View {
        ZStack {
            Image("memorise-header-bottom")
                .resizable()
            
            Image("memorise-header-top")
                .offset(y: -15)
            
            HStack {
                Image(vm.scoreImageName)
                
                HStack(alignment: .firstTextBaseline, spacing: 0) {
                    Image("memorise-counter-divider")
                        .resizable()
                        .scaledToFit()
                        .frame(maxHeight: 20, alignment: .bottom)
                        .padding(.trailing, 0)
                    Image(vm.endScoreImageName)
                        .resizable()
                        .scaledToFit()
                        .frame(maxHeight: 20, alignment: .bottom)
                }
                .frame(width: 40, height: 20, alignment: .bottom)
                .offset(y: 10)
            }
            .offset(x: 20)
        }
    }
}

struct MemoriseScoreCounterView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.gray
            
            MemoriseScoreCounterView(game: MemoriseModel())
        }
    }
}
