import Foundation
import SwiftUI
import Combine

class MemoriseScoreCounterVM: ObservableObject {
    
    @Published var score: Int = 0
    @Published var scoreImageName = "memorise-counter-number-0"
    @Published var endScoreImageName = "memorise-counter-number-15"
    
    var subscriptions = Set<AnyCancellable>()
    
    init (gameScore score: AnyPublisher<Int, Never>) {
        // It's needed because published value cannot be publish from MemoriseModel directly to the ScoreView via ScoreViewModel
        score
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: {
                self.score = $0
                self.scoreImageName = "memorise-counter-number-\($0)"
            })
            .store(in: &subscriptions)
    }
}
