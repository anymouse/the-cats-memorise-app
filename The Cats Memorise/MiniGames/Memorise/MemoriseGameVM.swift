import Foundation
import Combine

class MemoriseGameVM: ObservableObject {
    var deck: [MemoriseCardVM] = []
    
    @Published var game: MemoriseModel = MemoriseModel()
    @Published var gameInPlayMode: Bool = false
    
    var subscriptions = Set<AnyCancellable>()
    
    init () {
        var id: Int = 0
        while id < 15 {
            let card1 = MemoriseCardVM(
                game: game,
                pairId: id,
                withFrontImageName: "memorise-card-\(id)")
            let card2 = MemoriseCardVM(
                game: game,
                pairId: id,
                withFrontImageName: "memorise-card-\(id)")
            deck.append(contentsOf: [card1, card2])
            id += 1
        }
        
        let gameMode = self.game.$gamePlay.eraseToAnyPublisher()
        gameMode
            .sink(receiveValue: {
                self.gameInPlayMode = $0
            })
            .store(in: &subscriptions)
    }
            
    func getCards() -> [MemoriseCardVM] {
        deck.shuffle()
        return deck
    }
    
    func startGame() {
        self.game.start()
    }
}
