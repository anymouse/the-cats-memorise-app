import Foundation
import SwiftUI
import Combine

class MemoriseCardVM: ObservableObject, Identifiable, Equatable {
    static func == (lhs: MemoriseCardVM, rhs: MemoriseCardVM) -> Bool {
        return lhs === rhs
    }
    
    var game: MemoriseModel
    
    var id = UUID()
    var pairIdentifier: Int
    var frontImageName: String
    var hidden: Bool = false
    
    @Published var cardState: CardAppearance = CardState.back.state
    
    func select() {
        if (!hidden) {
            cardState = CardState.front(frontImageName).state
            game.matcher?.send(self)
        }
    }
    
    func flipBack() {
        cardState = CardState.back.state
    }
    
    func hide() {
        cardState = CardState.removed.state
        hidden = true
    }
    
    func successMatch() {
        cardState = CardState.match(frontImageName).state
    }
    
    struct CardAppearance {
        var imageName: String
        var strokeColor: Color
        var strokeWidth: CGFloat
        var background: Color
        var opacity: Double = 1.0
    }
    
    enum CardState {
        case back
        case front(String)
        case match(String)
        case removed
    
        var state: CardAppearance {
                switch self {
                case .front(let imageName):
                    return CardAppearance(imageName: imageName, strokeColor: Color("mini-game-dark-purple"), strokeWidth: 3, background: Color("mini-game-pink"))
                case .match(let imageName):
                    return CardAppearance(imageName: imageName, strokeColor: Color("mini-game-yellow"), strokeWidth: 5, background: Color("mini-game-pink"))
                case .back:
                    return CardAppearance(imageName: "memorise-card-back", strokeColor: Color.white, strokeWidth: 3, background: Color("mini-game-dark-purple"))
                case .removed:
                    return CardAppearance(imageName: "memorise-card-back", strokeColor: Color.white, strokeWidth: 0, background: Color.white, opacity: 0.0)
            }
        }
    }
    
    init (
        game currentGame: MemoriseModel,
        pairId identifier: Int,
        withFrontImageName imageName: String
    ) {
        game = currentGame
        pairIdentifier = identifier
        frontImageName = imageName
    }
}
