import SwiftUI

struct MemoriseCardView: View {
        
    @ObservedObject var vm: MemoriseCardVM
        
    var body: some View {
        Button(action: {
            vm.select()
        }, label: {
            ZStack {
                Image(vm.cardState.imageName)
                    .padding(2)
                    .overlay(RoundedRectangle(cornerRadius: 3)
                                .stroke(vm.cardState.strokeColor, lineWidth: vm.cardState.strokeWidth))
                    .background(vm.cardState.background)
                    .opacity(vm.cardState.opacity)
            }
        })
    }
}

struct MemoriseCardView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.gray
            
            MemoriseCardView(vm: memoriseCardDummy)
        }
    }
}
