//
//  MemoriesGameModel.swift
//  The Cats Memorise
//
//  Created by entymon on 11/04/2021.
//  Copyright © 2021 anymouse. All rights reserved.
//

import Foundation
import Combine

// Single Game State
final class MemoriseModel: ObservableObject {
    
    private var subscriptions = Set<AnyCancellable>()
    
    var gameWinnerScore: Int = 15
    var matcher: PassthroughSubject<MemoriseCardVM, Never>?
    var scoreCounter: PassthroughSubject<Int, Never>?
    var gameStatusPublisher: PassthroughSubject<Bool, Never>?
    
    @Published var gameScore: Int = 0
    @Published var gamePlay: Bool = false
    
    init() {
        matcher = matchingSubscriber()
        scoreCounter = scoreUpdateSubscriber()
        gameStatusPublisher = gameStatusUpdateSubscriber()
    }
    
    func start() {
        gameScore = 0
    }
    
    var cardsInPair: (MemoriseCardVM?, MemoriseCardVM?) = (nil, nil)
    
    func gameStatusUpdateSubscriber() -> PassthroughSubject<Bool, Never> {
        let subject = PassthroughSubject<Bool, Never>()
        subject
            .sink(receiveValue: { status in
                self.gamePlay = status
                if status {
                    self.start()
                }
            })
            .store(in: &subscriptions)
        
        return subject
    }
    
    func scoreUpdateSubscriber() -> PassthroughSubject<Int, Never> {
        let subject = PassthroughSubject<Int, Never>()
        subject
            .sink(receiveValue: { score in
                self.gameScore = self.gameScore + score
            })
            .store(in: &subscriptions)
        
        return subject
    }
    
    func matchingSubscriber() -> PassthroughSubject<MemoriseCardVM, Never> {
        let publisher = PassthroughSubject<MemoriseCardVM, Never>()
        let serialQueue = DispatchQueue(label: "Serial Queue")
        let subscriber = publisher
            .removeDuplicates()
            .collect(2)
            .receive(on: serialQueue)
            .map { collection in
                return (collection[0], collection[1])
            }
            .filter { cards in
                return cards.0.id != cards.1.id
            }
            .share()
            
        subscriber
            .filter { cards in
                return cards.0.pairIdentifier == cards.1.pairIdentifier
            }
            .handleEvents(receiveOutput: { cards in
                cards.0.successMatch()
                cards.1.successMatch()
                if self.gamePlay == true {
                    self.scoreCounter?.send(1)
                }
            })
            .delay(for: .seconds(0.3), scheduler: DispatchQueue.main)
            .sink(receiveValue: { cards in
                cards.0.hide()
                cards.1.hide()
            })
            .store(in: &subscriptions)
        
        subscriber
            .filter { cards in
                return cards.0.pairIdentifier != cards.1.pairIdentifier
            }
            .delay(for: .seconds(0.5), scheduler: DispatchQueue.main)
            .sink(receiveValue: { cards in
                cards.0.flipBack()
                cards.1.flipBack()
            })
            .store(in: &subscriptions)

        return publisher
    }
        
    func matching(pair cards: (MemoriseCardVM, MemoriseCardVM)) {
                
        let card1 = cards.0
        let card2 = cards.1
        
        if (card1.id != card2.id) {
            // Second selection
            if (card1.pairIdentifier == card2.pairIdentifier) {
    
                card1.successMatch()
                card2.successMatch()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    card1.hide()
                    card2.hide()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    card1.flipBack()
                    card2.flipBack()
                }
            }
        }
    }
}
